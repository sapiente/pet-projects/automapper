[![build status](https://gitlab.com/sapientes-php/automapper/badges/master/build.svg)](https://gitlab.com/sapientes-php/automapper/commits/master)
[![coverage report](https://gitlab.com/sapientes-php/automapper/badges/master/coverage.svg)](https://gitlab.com/sapientes-php/automapper/commits/master)

# Installation
`composer require sapientes/automapper`

# GUIDE

[WIKI](https://gitlab.com/sapientes-php/automapper/wikis/)
