<?php
namespace Sapientes\Automapper\Tests\Sanitizers\String;

use Sapientes\Automapper\Sanitizers\String\Trim;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class TrimTest extends \PHPUnit_Framework_TestCase {
    
    /** @var  Trim */
    protected $sanitizer;
    
    protected function setUp() {
        $this->sanitizer = new Trim();
    }
    
    /**
     * @dataProvider optionsProvider
     */
    public function testScalar($expected, $source, $side, $chars) {
        $source = new Source($source);
    
        $options = collect(['side' => $side]);
        if($chars) $options->put('chars', $chars);
        
        $this->sanitizer->sanitize($source, $options);
    
        $this->assertEquals($expected, $source->getSource());
    }
    
    /**
     * @dataProvider optionsProvider
     */
    public function testNestedSourceObject($expected, $source, $side, $chars) {
        $property = 'nested';
        $source = new Source((object) [$property => $source]);
        
        $options = collect(['source' => $property, 'side' => $side]);
        if($chars) $options->put('chars', $chars);
        
        $this->sanitizer->sanitize($source, $options);
        
        $this->assertEquals($expected, $source->getSource()->$property);
    }
    
    /**
     * @dataProvider optionsProvider
     */
    public function testNestedSourceArray($expected, $source, $side, $chars) {
        $key = 'nested';
        $source = new Source([$key => $source]);
        
        $options = collect(['source' => $key, 'side' => $side]);
        if($chars) $options->put('chars', $chars);
        
        $this->sanitizer->sanitize($source, $options);
        
        $this->assertEquals($expected, $source->getSource()[$key]);
    }
    
    public function optionsProvider() {
        $text = str_random(16);
        return [
            ["{$text} ", " {$text} ", 'left', null],
            ["{$text} ", " l{$text} ", 'left', 'chars' => ' l'],
            [" {$text}", " {$text} ", 'right', null],
            [" {$text}", " {$text}l ", 'right', 'chars' => ' l'],
            ["{$text}", " {$text} ", 'both', null],
            ["{$text}", " l{$text}l ", 'both', 'chars' => ' l'],
        ];
    }
}
