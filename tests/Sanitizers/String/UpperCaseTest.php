<?php
namespace Sapientes\Automapper\Tests\Sanitizers\String;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Sapientes\Automapper\Sanitizers\String\UpperCase;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class UpperCaseTest extends \PHPUnit_Framework_TestCase {
    
    /** @var  UpperCase */
    protected $sanitizer;
    /** @var  string */
    protected $randomText;
    
    protected function setUp() {
        $this->sanitizer = new UpperCase();
        $this->randomText = str_random(16);
    }
    
    public function testScalar() {
        $source = new Source($this->randomText);
        $this->sanitizer->sanitize($source, collect());
        
        $this->assertEquals(Str::upper($this->randomText), $source->getSource());
    }
    
    public function testNestedSourceObject() {
        $property = 'nested';
        $source = new Source((object) [$property => $this->randomText]);
        $this->sanitizer->sanitize($source, collect(['source' => $property]));
    
        $this->assertEquals(Str::upper($this->randomText), $source->getSource()->$property);
    }
    
    public function testNestedSourceArray() {
        $key = 'nested';
        $source = new Source([$key => $this->randomText]);
        $this->sanitizer->sanitize($source, collect(['source' => $key]));
        
        $this->assertEquals(Str::upper($this->randomText), $source->getSource()[$key]);
    }
}
