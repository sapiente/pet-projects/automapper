<?php
namespace Sapientes\Automapper\Tests\MappingStrategy;

use Camel\Format\CamelCase;
use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\MappingStrategy\NameStrategy;
use Sapientes\Automapper\Source;
use Sapientes\Automapper\Tests\MappingStrategy\NameStrategy\Destination;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class NameStrategyTest extends \PHPUnit_Framework_TestCase {
    /** @var  NameStrategy */
    protected $strategy;
    /** @var  \ReflectionClass */
    protected $destination;
    
    protected function setUp() {
        $this->strategy = new NameStrategy(new CamelCase());
        $this->destination = new \ReflectionClass(Destination::class);
    }
    
    public function testNoMap() {
        $this->assertFalse($this->strategy->shouldMap(new Source(null), $this->destination->getProperty('_noMap')));
    }
    
    /**
     * @dataProvider sourceProvider
     */
    public function testMapping(Source $source) {
        $values = collect();
        foreach ($this->destination->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if(! $this->strategy->shouldMap($source, $property)) continue;
            
            $values->put($property->getName(), $this->strategy->getValue($source, $property, new Mapper($this->strategy)));
        }
        
        $this->assertEquals('Person Name', $values->get('personName'));
        $this->assertSame($expected = new \stdClass(), $values->get('noMap', $expected));
    }
    
    public function sourceProvider() {
        return [
            [
                new Source(require realpath(__DIR__ . '/NameStrategy/array_source.php'))
            ],
            [
                new Source(new \Sapientes\Automapper\Tests\MappingStrategy\NameStrategy\Source())
            ]
        ];
    }
}
