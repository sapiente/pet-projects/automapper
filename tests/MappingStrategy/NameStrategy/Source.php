<?php
namespace Sapientes\Automapper\Tests\MappingStrategy\NameStrategy;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class Source {
    /** @var PersonSource  */
    public $person;
    
    /**
     * Source constructor.
     */
    public function __construct() {
        $this->person = new PersonSource('Person Name');
    }
}