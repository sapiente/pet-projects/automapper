<?php
namespace Sapientes\Automapper\Tests\MappingStrategy\NameStrategy;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class Destination {
    public $_noMap;
    public $personName;
}