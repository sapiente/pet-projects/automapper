<?php
namespace Sapientes\Automapper\Tests\MappingStrategy;

use Doctrine\Common\Annotations\AnnotationReader;
use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\MappingStrategy\AnnotationStrategy;
use Sapientes\Automapper\Source;
use Sapientes\Automapper\Tests\MappingStrategy\AnnotationStrategy\CompanyDestination;
use Sapientes\Automapper\Tests\MappingStrategy\AnnotationStrategy\Destination;
use Sapientes\Automapper\Tests\MappingStrategy\AnnotationStrategy\PersonDestination;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class AnnotationStrategyTest extends \PHPUnit_Framework_TestCase {
    /** @var  AnnotationStrategy */
    protected $strategy;
    /** @var  \ReflectionClass */
    protected $destination;
    
    protected function setUp() {
        $this->strategy = new AnnotationStrategy(new AnnotationReader());
        $this->destination = new \ReflectionClass(Destination::class);
    }
    
    public function testNoMap() {
        $this->assertFalse($this->strategy->shouldMap(new Source(null), $this->destination->getProperty('noMap')));
    }
    
    /**
     * @dataProvider sourceProvider
     */
    public function testMapping(Source $source) {
        $values = collect();
        foreach ($this->destination->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if(! $this->strategy->shouldMap($source, $property)) continue;
    
            $values->put($property->getName() ,$this->strategy->getValue($source, $property, new Mapper($this->strategy)));
        }
        
        $expectedPerson = new PersonDestination(['name' => 'Person Name', 'age' => 100]);
        $this->assertEquals($expectedPerson, $values->get('person'));
        
        $expectedCompanies = [
            new CompanyDestination(['name' => 'First Company']),
            new CompanyDestination(['name' => 'Second Company']),
        ];
        $this->assertEquals($expectedCompanies, $values->get('companies'));
        
        $this->assertEquals('Just some text', $values->get('text'));
        $this->assertEquals((object) ['property' => 'value'], $values->get('object'));
        $this->assertEquals(phpversion(), $values->get('phpver'));
    }
    
    public function sourceProvider() {
        return [
            [
                new Source(require realpath(__DIR__ . '/AnnotationStrategy/array_srouce.php'))
            ],
            [
                new Source(new \Sapientes\Automapper\Tests\MappingStrategy\AnnotationStrategy\Source())
            ]
        ];
    }
}
