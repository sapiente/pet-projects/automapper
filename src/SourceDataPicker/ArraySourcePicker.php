<?php
namespace Sapientes\Automapper\SourceDataPicker;

use TRex\Reflection\CallableReflection;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class ArraySourcePicker implements SourcePicker {
    
    /**
     * @inheritdoc
     */
    public function &pickValue(&$source, string $key) {
        if(! array_key_exists($key, $source)) {
            throw new \RuntimeException(sprintf("Key '%s' does not exists in array", $key));
        }
    
        return $source[$key];
    }
    
    /**
     * @inheritdoc
     */
    public function getSourceTypeName(): string {
        return 'array';
    }
    
    /**
     * @inheritdoc
     */
    public function pickFunction($source, string $name): CallableReflection {
        return new CallableReflection($this->pickValue($source, $name));
    }
}