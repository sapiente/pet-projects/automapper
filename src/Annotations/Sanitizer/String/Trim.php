<?php
namespace Sapientes\Automapper\Annotations\Sanitizer\String;

use Doctrine\Common\Annotations\Annotation\Enum;
use Sapientes\Automapper\Annotations\Sanitizer\AnnotationTrait;
use Sapientes\Automapper\Annotations\Sanitizer\SanitizerAnnotation;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class Trim implements SanitizerAnnotation {
    
    use AnnotationTrait;
    
    /** @var  string */
    public $chars;
    /** @var  Enum({"left", "right", "both"}) */
    public $side;
    
    /**
     * @inheritDoc
     */
    public function getSanitizerName(): string {
        return \Sapientes\Automapper\Sanitizers\String\Trim::class;
    }
}