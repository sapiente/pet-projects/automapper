<?php
namespace Sapientes\Automapper\MappingStrategy\AnnotationStrategy;

use Doctrine\Common\Annotations\Reader;
use Sapientes\Automapper\Annotations\Mapping\FromFunction;
use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\Source;
use TRex\Reflection\CallableReflection;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class FunctionStrategy implements AnnotationStrategy {
    
    /** @var  Reader */
    protected $reader;
    
    /**
     * PropertyStrategy constructor.
     *
     * @param Reader $reader
     */
    public function __construct(Reader $reader) {
        $this->reader = $reader;
    }
    
    /**
     * @inheritdoc
     */
    public function getValue(Source $source, \ReflectionProperty $property, Mapper $mapper) {
        /** @var FromFunction $annotation */
        $annotation = $this->reader->getPropertyAnnotation($property, FromFunction::class);
        $function = $this->getFunction($source, $annotation);
        return $function->invokeArgs($annotation->params);
    }
    
    /**
     * Get callable function.
     *
     * @param Source       $source
     * @param FromFunction $annotation
     *
     * @return CallableReflection
     */
    protected function getFunction(Source $source, FromFunction $annotation) : CallableReflection {
        return is_null($annotation->source) ?
            new CallableReflection($annotation->function) :
            $source->pickFunction($annotation->source, $annotation->function);
    }
    
    /**
     * @inheritdoc
     */
    public function canBeUsed(Source $source, \ReflectionProperty $property): bool {
        return $this->reader->getPropertyAnnotation($property, FromFunction::class) !== null;
    }
}