<?php
namespace Sapientes\Automapper\MappingStrategy\AnnotationStrategy;

use Doctrine\Common\Annotations\Reader;
use Sapientes\Automapper\Annotations\Mapping\FromMember;
use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class MemberStrategy implements AnnotationStrategy {
    
    /** @var  Reader */
    protected $reader;
    
    /**
     * PropertyStrategy constructor.
     *
     * @param Reader $reader
     */
    public function __construct(Reader $reader) {
        $this->reader = $reader;
    }
    
    /**
     * @inheritdoc
     */
    public function getValue(Source $source, \ReflectionProperty $property, Mapper $mapper) {
        /** @var FromMember $annotation */
        $annotation = $this->reader->getPropertyAnnotation($property, FromMember::class);
        return $source->pickValue($annotation->source ?? '.');
    }
    
    /**
     * @inheritdoc
     */
    public function canBeUsed(Source $source, \ReflectionProperty $property): bool {
        return $this->reader->getPropertyAnnotation($property, FromMember::class) !== null;
    }
}