<?php
namespace Sapientes\Automapper\MappingStrategy\AnnotationStrategy;

use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
interface AnnotationStrategy {
    /**
     * Test if strategy can be used
     *
     * @param  Source             $source
     * @param \ReflectionProperty $property
     *
     * @return bool
     */
    public function canBeUsed(Source $source, \ReflectionProperty $property) : bool;
    
    /**
     * Get value from source for given property.
     *
     * @param Source              $source
     * @param \ReflectionProperty $property
     * @param Mapper              $mapper
     *
     * @return mixed
     */
    public function getValue(Source $source, \ReflectionProperty $property, Mapper $mapper);
}