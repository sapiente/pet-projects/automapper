<?php
namespace Sapientes\Automapper\Sanitizers\String;

use Illuminate\Support\Collection;
use Sapientes\Automapper\Sanitizers\Sanitizer;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class Append implements Sanitizer {
    
    /**
     * @inheritdoc
     */
    public function sanitize(Source $source, Collection $options) {
        $source->transformValue($options->get('source') ?? '', function($value) use ($options) {
            return  $value . $options->get('text');
        });
    }
}